import createSagaMiddleware  from 'redux-saga';
import logger from 'redux-logger';
import { applyMiddleware, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import history from 'history/createBrowserHistory';

// REDUCER
import reducer from './reducer';
// SAGA
import { saga as rootSaga } from './saga';

// HISTORY
export const appHistory = history();

// MIDDLEWARES
const customRouterMiddleware = routerMiddleware(appHistory);
const sagaMiddleware = createSagaMiddleware();

// ENHANCER
const enhancer = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(sagaMiddleware, customRouterMiddleware);
  } else {
    return applyMiddleware(sagaMiddleware, customRouterMiddleware, logger);
  }
};

// STORE
const store = createStore(reducer, enhancer());
sagaMiddleware.run(rootSaga);

export default store;