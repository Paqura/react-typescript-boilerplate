import * as React from 'react';
import { Switch, Route, withRouter } from 'react-router';

// FAKE ROUTES
const App = () => <h1>App</h1>
const About = () => <h1>About</h1>

class Root extends React.Component<any, any> {
  public render() {
    return(
      <Switch>
        <Route 
          exact={true} 
          path='/' 
          component={App}
        />
        <Route 
          exact={true} 
          path='/about' 
          component={About}
        />
      </Switch>
    )
  }
};

export default withRouter(Root);