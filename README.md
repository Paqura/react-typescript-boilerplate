## React-typescript-boilerplate version: 1.0
---

#### Технологии:

0) Typescript;
1) React;
2) React-router;
3) React-router-redux@next;
4) React-redux;
5) Redux;
6) Redux-saga;
7) Styled-components;
8) Jest;
9) Enzyme;

#### Установка:

```
1) yarn;
1.1) если не хочется через yarn, удалить yarn.lock и ставить через npm install;
2) yarn start - запуск
3) yarn test  - тесторование
4) yarn build - собственно, вжух-вжух и в продакшн
```